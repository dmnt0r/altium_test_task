﻿using CommandLine;
using LargeFilesSorter;
using LargeFilesSorter.Utils;
using System;

namespace FileSorter
{
    public class Args
    {
        [Option('o', "output", Required = true, HelpText = "Output file")]
        public string OutputFile { get; set; }

        [Option('i', "input", Required = true, HelpText = "Input file")]
        public string InputFile { get; set; }

        [Option('t', "temp-folder", Required = false, HelpText = "A folder to use for storing temporary files", Default = null)]
        public string TempFolder { get; set; }

        [Option('b', "max-breadth", Required = false, HelpText = "Max merge breadth", Default = 100)]
        public int MaxMergeBreadth { get; set; }

        [Option('m', "max-chunk-size", Required = false, HelpText = "Max size of file chunk to be sorted in memory", Default = "256MB")]
        public string MaxInMemorySortedChunk { get; set; }

        [Option('d', "disk-buffer-size", Required = false, HelpText = "Disk IO buffer size", Default = "10MB")]
        public string DiskIOBufferSize { get; set; }

        public SortingArgs ToSortingArgs()
        {
            var chunkSize = DataSizeParser.ToBytes(MaxInMemorySortedChunk);

            var bufferSize = Convert.ToInt32(DataSizeParser.ToBytes(DiskIOBufferSize));

            if (chunkSize < 2048)
            {
                throw new ArgumentException("max-chunk-size should be greater than 2KB");
            }

            if (bufferSize < 4096)
            {
                throw new ArgumentException("disk-buffer-size should be greater than system's 4KB");
            }

            if (MaxMergeBreadth <= 1)
            {
                throw new ArgumentException("max-breadth should be greater than 1");
            }

            if (MaxMergeBreadth > 511) // 512 is max open files for a process in windows by default.  1024 fo linux.
            {
                throw new ArgumentException("max-breadth should be lower than 511");
            }

            return new SortingArgs()
            {
                SourceFile = InputFile,
                DestinationFile = OutputFile,
                TempDir = TempFolder,
                MaxMergeBreadth = MaxMergeBreadth,
                ChunkSize = chunkSize
            };
        }
    }

}
