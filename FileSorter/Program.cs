﻿using CommandLine;
using LargeFilesSorter;
using OpenTelemetry;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using System;
using System.Diagnostics;

namespace FileSorter
{
    class Program
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Args>(args).WithParsed(Run);
        }

        static void Run(Args args)
        {
            try
            {


                var serviceName = typeof(Program).FullName;
                var serviceVersion = typeof(Program).Assembly.GetName().Version.ToString();
                using var tracer =
                    Sdk
                    .CreateTracerProviderBuilder()
                    .AddSource(serviceName)
                    .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName))
                    .AddConsoleExporter(x =>
                    {
                        x.Targets = OpenTelemetry.Exporter.ConsoleExporterOutputTargets.Console;
                    })
                    .Build();

                var mySource = new ActivitySource(serviceName);
                Core.ConfigureTracingActivitySource(mySource);

                if (string.IsNullOrWhiteSpace(args.TempFolder))
                {
                    args.TempFolder = null;
                }



                using var activity = mySource.StartActivity("Program");

                Core.SortLargeFile(args.ToSortingArgs());
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Failed to sort:\n{e}");
                Environment.ExitCode = -1;
            }
        }
    }
}
