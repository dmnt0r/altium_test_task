﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace LargeFilesSorter
{

    public static class Core
    {
        public static ActivitySource Tracing { get; private set; } = new ActivitySource("Core"); 
        public static void ConfigureTracingActivitySource(ActivitySource src)
        {
            Tracing = src;
        }

        public static readonly IComparer<(long, string)> Comparer = new MyComparer();

        private static string TempFileName(string outputDir = null)
        {
            var dir = Path.Combine(outputDir ?? Path.GetTempPath());

            var fname = Path.Combine(dir, Path.GetRandomFileName());
            return fname;
        }

        // merges pre-sorted items
        public static IEnumerable<(long, string)> Merge(this IEnumerable<IEnumerable<(long, string)>> data)
        {
            return data.Merge(Comparer);
        }

        // initial parsing and sorting func
        public static List<(long, string)> ParseAndSort(List<string> lines)
        {
            using var activity = Tracing.StartActivity("ParseAndSort");
            var it = lines.Select(Input.ParseLine).ToList();
            it.Sort(Comparer);
            return it;
        }

        // persists the presorted ienumerable to a temporary file, returns the filename
        public static string WriteToTemporaryFile(this IEnumerable<(long, string)> data, string outputDir, Activity parent = null, int IOBufferSize = SortingArgs.DefaultIOBufferSize)
        {
            using var activity = Tracing.StartActivity("PersistTemporaryFile");
            if (parent != null) activity.SetParentId(parent.Id);
            var fname = TempFileName(outputDir);
            using (var f = File.Open(fname, FileMode.Create, FileAccess.Write))
            using (var b = new BufferedStream(f, IOBufferSize))
            {
                data.SerializeItems(b);
                b.Flush();
            }

            activity.AddTag("GC.GetTotalMemory", GC.GetTotalMemory(true).ToString());
            return Path.GetFullPath(fname);
        }

        private static IEnumerable<(long, string)> ReadFromTemporaryFile(string file, Activity parent = null, int IOBufferSize = SortingArgs.DefaultIOBufferSize)
        {
            using var activity = Tracing.StartActivity("ReadTempFile");
            if (parent != null) activity.SetParentId(parent.Id);
            using (var f = File.Open(file, FileMode.Open, FileAccess.Read))
            using (var b = new BufferedStream(f, IOBufferSize))
            {
                foreach (var i in Serialization.DeserializeItems(b))
                {
                    yield return i;
                }
            }
        }

        // performs large file sorting
        public static void SortLargeFile(SortingArgs args, Activity parent = null)
        {
            if(args.TempDir != null)
            {
                Directory.CreateDirectory(args.TempDir);
            }

            using var activity = Tracing.StartActivity("SortLargeFile");
            if (parent != null) activity.SetParentId(parent.Id);
            activity.AddTag("chunk-size", args.ChunkSize);
            activity.AddTag("max-merge-breadth", args.MaxMergeBreadth);

            var queued = new Queue<string>();
            var mergeTasks =
                Input.ReadChunked(args)
                    .AsParallel()
                    .AsUnordered()
                    .WithDegreeOfParallelism(args.ChunksSortingDop)
                    .Select(x => ParseAndSort(x).WriteToTemporaryFile(args.TempDir, activity))                    
                    .WithMergeOptions(ParallelMergeOptions.NotBuffered)
                    .AsEnumerable();

            foreach (var f in mergeTasks)
            {
                queued.Enqueue(f);
            }

            while (true)
            {
                ProcessTask mergeTask = null;
                bool isLastMergeTask = false;

                if (queued.Count > 0)
                {
                    mergeTask = new ProcessTask
                    {
                        InputFiles = new List<string>()
                    };

                    // try to consume queue evenly
                    var mergeCount = args.MaxMergeBreadth;
                    if (queued.Count > args.MaxMergeBreadth)
                    {
                        mergeCount = Math.Min(Math.Max(queued.Count / 2, 2), args.MaxMergeBreadth);
                    }

                    for (var i = 0; i < mergeCount; i++)
                    {
                        if (queued.TryDequeue(out string sortedFile))
                        {
                            mergeTask.InputFiles.Add(sortedFile);
                        }
                        else
                        {
                            break;
                        }
                    }

                    isLastMergeTask = queued.Count == 0;
                    // no merged files to come - final result we be converted to output
                }


                if (mergeTask != null)
                {
                    var enumerables =
                        mergeTask.InputFiles.Count == 1 ?
                        ReadFromTemporaryFile(mergeTask.InputFiles.Single(), activity, args.DiskIOBufferSize) :
                        mergeTask.InputFiles.Select(x => ReadFromTemporaryFile(x, activity, args.DiskIOBufferSize)).ToList().Merge();

                    if (isLastMergeTask)
                    {
                        using (var outputActivity = Tracing.StartActivity("Output"))
                        {
                            outputActivity.SetParentId(activity.Id);
                            using (var f = File.Open(args.DestinationFile, FileMode.Create, FileAccess.Write))
                            using (var b = new BufferedStream(f, args.DiskIOBufferSize))
                            using (var s = new StreamWriter(b, System.Text.Encoding.Unicode))
                            {
                                var lines = enumerables.Select((item) =>
                                {
                                    var (num, str) = item;
                                    return $"{num}.{str}";
                                });
                                foreach (var  line in lines)
                                {
                                    s.WriteLine(line);
                                }
                            }}

                        activity.AddTag("GC.GetTotalMemory", GC.GetTotalMemory(true).ToString());
                    }
                    else
                    {
                        var newMergedFile = WriteToTemporaryFile(enumerables, args.TempDir, activity, args.DiskIOBufferSize);
                        queued.Enqueue(newMergedFile);
                    }

                    // delete processed temporary files
                    foreach (var tempFile in mergeTask.InputFiles)
                    {
                        File.Delete(tempFile);
                    }
                }
                else
                {
                    return;
                }
            }
        }
    }
}
