﻿using System.Collections.Generic;
using System.Linq;

namespace LargeFilesSorter
{
    public static class EnumerableUtils
    {

        // chunks ienumerable by size
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> values, int chunkSize)
        {
            using (var enumerator = values.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    yield return GetChunk(enumerator, chunkSize).ToList();
                }
            }
        }

        private static IEnumerable<T> GetChunk<T>(IEnumerator<T> enumerator, int chunkSize)
        {
            do
            {
                yield return enumerator.Current;
            } while (--chunkSize > 0 && enumerator.MoveNext());
        }

        // merge presorted ienumerables using specified comparer
        // https://stackoverflow.com/questions/2767007/most-efficient-algorithm-for-merging-sorted-ienumerablet
        public static IEnumerable<T> Merge<T>(this IEnumerable<IEnumerable<T>> sortedData, IComparer<T> comparer)
        {
            var items = sortedData.Select(sequence => sequence.GetEnumerator()).Where(e => e.MoveNext())
                .OrderBy(x => x.Current, comparer)
                .ToList();

            while (items.Any())
            {
                var next = items[0];
                yield return next.Current;
                items.RemoveAt(0);
                if (next.MoveNext())
                {
                    var insertIx = 0;
                    for (; insertIx < items.Count; insertIx++)
                    {
                        if (comparer.Compare(next.Current, items[insertIx].Current) < 0)
                        {
                            items.Insert(insertIx, next);
                            break;
                        }
                    }

                    if (insertIx == items.Count)
                    {
                        items.Add(next);
                    }
                }
                else next.Dispose();
            }
        }
    }
}
