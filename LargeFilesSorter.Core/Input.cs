﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LargeFilesSorter
{
    public static class Input
    {
        public const long BytesIn256MB = 268435456;

        public static IEnumerable<List<string>> ReadChunked(SortingArgs args)
        {
            var buffer = new List<string>();
            var prevPosition = 0L;
            using (var f = File.Open(args.SourceFile, FileMode.Open, FileAccess.Read))
            using (var b = new BufferedStream(f, Convert.ToInt32(Math.Min(args.ChunkSize, BytesIn256MB))))
            using (var s = new StreamReader(b, Encoding.Unicode))
            {
                while (true)
                {
                    var str = s.ReadLine();
                    if (str == null) // reached end of stream
                    {
                        if (buffer.Count > 0)
                        {
                            yield return buffer;
                        }
                        break;
                    }
                    else
                    {
                        if ((s.BaseStream.Position - prevPosition) > args.ChunkSize) // reached end of stream chunk
                        {
                            prevPosition = s.BaseStream.Position;
                            var it = buffer;
                            buffer = new List<string>(); // init new buffer
                            yield return it; // yield accumulated
                        }
                    }
                    buffer.Add(str);
                }
            }
        }


        private static char[] separator = new[] { '.' };
        public static (long, string) ParseLine(string line)
        {
            var parts = line.Split(separator, 2);
            return (long.Parse(parts[0]), parts[1]);
        }

    }
}
