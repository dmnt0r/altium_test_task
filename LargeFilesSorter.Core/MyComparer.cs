﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace LargeFilesSorter
{
    public class MyComparer : IComparer<(long, string)>
    {
        private StringComparer Strings = StringComparer.Ordinal;
        private Comparer<long> Numbers = Comparer<long>.Default;

        public int Compare([AllowNull] (long, string) x, [AllowNull] (long, string) y)
        {
            var strCompareResult = Strings.Compare(x.Item2, y.Item2);
            if (strCompareResult == 0) // equal strings
            {
                return Numbers.Compare(x.Item1, y.Item1);
            }
            else
            {
                return strCompareResult;
            }
        }
    }
}
