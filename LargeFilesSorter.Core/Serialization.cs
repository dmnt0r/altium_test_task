﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LargeFilesSorter
{
    public static class Serialization
    {

        public static void SerializeItems(this IEnumerable<(long, string)> data, Stream f)
        {
            foreach (var item in data)
            {
                var num = BitConverter.GetBytes(item.Item1);
                f.Write(num, 0, num.Length);
                var strBytes = Encoding.Unicode.GetBytes(item.Item2);
                var strLen = BitConverter.GetBytes(strBytes.Length);
                f.Write(strLen, 0, strLen.Length);
                f.Write(strBytes, 0, strBytes.Length);
            }
        }


        private static (long, string)? ReadItem(this Stream f)
        {
            var numberBytes = f.ReadFully(sizeof(long));

            if (numberBytes == null)
            {
                return null;
            }
            var num = BitConverter.ToInt64(numberBytes);

            var strLenBytes = f.ReadFully(sizeof(int));
            if (strLenBytes == null)
            {
                throw new EndOfStreamException();
            }
            var strLen = BitConverter.ToInt32(strLenBytes);

            var strBytes = f.ReadFully(strLen);
            if (strBytes == null)
            {
                throw new EndOfStreamException();
            }
            var str = Encoding.Unicode.GetString(strBytes);

            return (num, str);
        }

        public static IEnumerable<(long, string)> DeserializeItems(this Stream f)
        {
            var item = f.ReadItem();
            while (item != null)
            {
                yield return item.Value;
                item = f.ReadItem();
            }
        }

        private static byte[] ReadFully(this Stream f, int count)
        {
            var readCount = 0;
            var buffer = new byte[count];

            while (readCount < count)
            {
                var read = f.Read(buffer, readCount, count - readCount);

                if (read == 0 && readCount == 0)
                {
                    // we're at the end of stream
                    return null;
                }

                if (read == 0)
                {
                    // we've read a portion of expected data, but stream ended
                    throw new EndOfStreamException();
                }

                readCount += read;
            }

            return buffer;
        }
    }
}
