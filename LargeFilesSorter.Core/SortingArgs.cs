﻿namespace LargeFilesSorter
{
    public class SortingArgs
    {
        public const long BytesIn256MB = 268435456L;
        public const int DefaultIOBufferSize = 10485760; // 10MB

        public string SourceFile = null;
        public string TempDir = null;
        public int MaxMergeBreadth = 16;
        public string DestinationFile = null;
        public int ChunksSortingDop = 2;
        public long ChunkSize = BytesIn256MB;
        public int DiskIOBufferSize = DefaultIOBufferSize;
    }
}
