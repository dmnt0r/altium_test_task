﻿using Bogus;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LargeFilesSorter
{
    public static class Generator
    {
        private static Faker _faker = new Faker();
        private static List<string> _duplicates = new List<string>();
        private static string NextString(int minStringLength, int maxStringLength, double duplicateProbability, int dupBuffSize)
        {
            var isDuplicate = _faker.Random.Double() < duplicateProbability;
            string str;
            if (isDuplicate)
            {
                if (_duplicates.Any())
                {
                    var ix = _faker.Random.Int(0, _duplicates.Count - 1);
                    str = _duplicates[ix];
                }
                else
                {
                    str = _faker.Random.Utf16String(minStringLength, maxStringLength, true);
                }
            }
            else
            {
                str = _faker.Random.Utf16String(minStringLength, maxStringLength, true);
                if (_duplicates.Count < dupBuffSize)
                {
                    _duplicates.Add(str);
                }
            }

            if (!isDuplicate && _duplicates.Count > dupBuffSize / 2 && _faker.Random.Double() > duplicateProbability) // replace previous duplicate with new str
            {
                var ix = _faker.Random.Int(0, _duplicates.Count - 1);
                _duplicates[ix] = str;
            }

            return $" {str}";
        }
        public static string AsString(this (long, string) item)
        {
            var (num, str) = item;
            return $"{num}.{str}";
        }

        public static string GenerateLine(int minStringLength = 4, int maxStringLength = 15977, double duplicateProbability = 0.4, int dupBuffSize = 1000)
        {
            return GenerateItem(minStringLength, maxStringLength, duplicateProbability, dupBuffSize).AsString();
        }


        public static (long, string) GenerateItem(int minStringLength = 4, int maxStringLength = 15977, double duplicateProbability = 0.4, int dupBuffSize = 1000)
        {
            return (_faker.Random.Long(), NextString(minStringLength, maxStringLength, duplicateProbability, dupBuffSize));
        }

        public static void GenerateFile(string filePath, long maxSize, int minStringLength = 4, int maxStringLength = 15977, double duplicateProbability = 0.4, int dupBuffSize = 1000)
        {
            using (var f = File.Open(filePath, FileMode.Create, FileAccess.Write))
            using (var s = new StreamWriter(f, encoding: System.Text.Encoding.Unicode))
            {
                while (true)
                {
                    var str = GenerateLine(minStringLength, maxStringLength, duplicateProbability, dupBuffSize);
                    var strLength = System.Text.Encoding.Unicode.GetByteCount(str);

                    if (f.Length + strLength < maxSize)
                    {
                        s.WriteLine(str);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}
