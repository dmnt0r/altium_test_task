﻿using NUnit.Framework;
using System.Collections.Generic;

namespace LargeFilesSorter.Tests
{

    [TestFixture]
    public class ComparerTests
    {
        [Test]
        public void SortingComparesStringsFirstThenNums()
        {
            var sampleData = new List<(long, string)>()
            {
                (7, "a"),
                (6, "aa"),
                (5, "b"),
                (4, "ab"),
                (3, "ba"),
                (99, "ab"),
                (100, "ab"),
                (200, "a cab"),
                (200, "a cab")
            };

            var expected = new[] {
                (7, "a"),
                (200, "a cab"),
                (200, "a cab"),
                (6, "aa"),
                (4, "ab"),
                (99, "ab"),
                (100, "ab"),
                (5, "b"),
                (3, "ba"),
            };

            sampleData.Sort(Core.Comparer);

            CollectionAssert.AreEqual(expected, sampleData);


        }
    }
}
