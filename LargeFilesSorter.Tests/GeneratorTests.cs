﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LargeFilesSorter.Tests
{
    [TestFixture]
    public class GeneratorTests
    {

        [Test]
        public void GeneratedFileSizeIsRoughlySameAsRequested()
        {
            var GB = 1024L * 1024L * 1024L;
            var filename = $"{Path.GetRandomFileName()}.txt";
            var fullName = Path.GetFullPath(filename);
            Generator.GenerateFile(fullName, GB);
            var fi = new FileInfo(fullName);
            try
            {
                Assert.IsTrue(fi.Exists);
                Assert.IsTrue(Math.Abs(fi.Length - GB) < 20000); // max of typical buffer sizes and ours string length
            }
            finally
            {
                if (fi.Exists)
                {
                    fi.Delete();
                }
            }
        }

        [Test]
        public void GeneratedValidFileWithDuplicates()
        {
            var Size = 1024L * 1024L * 64L;
            var filename = $"{Path.GetRandomFileName()}.txt";
            var fullName = Path.GetFullPath(filename);
            Generator.GenerateFile(fullName, Size);
            var fi = new FileInfo(fullName);
            try
            {
                var data = File.ReadAllLines(fullName, System.Text.Encoding.Unicode).Select(x =>
                    x.Split(new char[] { '.' }, 2)
                ).Select(parts => (long.Parse(parts[0]), parts[1]));

                var freqs = new Dictionary<string, int>();
                foreach (var item in data)
                {
                    var (_, str) = item;
                    if (freqs.ContainsKey(str))
                    {
                        freqs[str]++;
                    }
                    else
                    {
                        freqs.Add(str, 1);
                    }
                }

                Assert.IsTrue(freqs.Any(x => x.Value > 1));
            }
            finally
            {
                if (fi.Exists)
                {
                    fi.Delete();
                }
            }
        }
    }
}
