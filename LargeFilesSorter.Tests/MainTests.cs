﻿using NUnit.Framework;
using OpenTelemetry;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LargeFilesSorter.Tests
{
    [TestFixture]
    public class MainTests
    {
        static readonly long GB = 1024L * 1024L * 1024L;
        private TracerProvider Tracer;

        [OneTimeSetUp]
        public void SetUpTracing()
        {
            var serviceName = GetType().FullName;
            var serviceVersion = GetType().Assembly.GetName().Version.ToString();

            Tracer =
                Sdk
                .CreateTracerProviderBuilder()
                .AddSource(serviceName)
                .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName))
                .AddConsoleExporter(x =>
                {
                    x.Targets = OpenTelemetry.Exporter.ConsoleExporterOutputTargets.Debug | OpenTelemetry.Exporter.ConsoleExporterOutputTargets.Console;
                })
                .Build();

            Core.ConfigureTracingActivitySource(new ActivitySource(serviceName));
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            Tracer?.Dispose();
        }

        [Test]
        public void CanReadInputData()
        {
            using var activity = Core.Tracing.StartActivity("CanReadInputData");
            var filename = $"{Path.GetRandomFileName()}.txt";
            var fullName = Path.GetFullPath(filename);
            var fi = new FileInfo(fullName);
            var lines = Enumerable.Range(1, 100000).Select(_ => Generator.GenerateLine()).ToArray();

            File.WriteAllLines(fullName, lines, Encoding.Unicode);
            try
            {
                CollectionAssert.AreEqual(lines, Input.ReadChunked(new SortingArgs { SourceFile = fullName, ChunkSize = 1024L * 1024L}).SelectMany(x => x));
            }
            finally
            {
                if (fi.Exists)
                {
                    fi.Delete();
                }
            }
        }


        [Test]
        public void CanMergePreSortedData()
        {
            using var activity = Core.Tracing.StartActivity("CanMergePreSortedData");
            var filename = $"{Path.GetRandomFileName()}.txt";
            var fullName = Path.GetFullPath(filename);
            var fi = new FileInfo(fullName);
            var items = Enumerable.Range(1, 10000).Select(_ => Generator.GenerateItem()).ToList();
            var lines = items.Select(x => x.AsString()).ToArray();
            items.Sort(Core.Comparer);

            File.WriteAllLines(fullName, lines, Encoding.Unicode);
            try
            {
                var sorted = Input.ReadChunked(new SortingArgs { SourceFile = fullName, ChunkSize = 128L * 1024L }).Select(Core.ParseAndSort).Merge().ToList();
                CollectionAssert.AreEqual(items, sorted);
            }
            finally
            {
                if (fi.Exists)
                {
                    fi.Delete();
                }
            }
        }

        [Test]
        public void CanSort1GBFile()
        {
            using var activity = Core.Tracing.StartActivity("CanSort1GBFile");
            var inputFile = $"{Path.GetRandomFileName()}.txt";
            var outputFile = $"{Path.GetRandomFileName()}.txt";
            try
            {
                Generator.GenerateFile(inputFile, GB);

                Core.SortLargeFile(new SortingArgs { SourceFile = inputFile, DestinationFile = outputFile });
                var isSorted = IsSortedFile(outputFile, activity);
                Assert.IsTrue(isSorted);
            }
            finally
            {
                if (File.Exists(inputFile))
                {
                    File.Delete(inputFile);
                }
                if (File.Exists(outputFile))
                {
                    File.Delete(outputFile);
                }
            }
        }

        [Test]
        public void CanSort10GbFile()
        {
            using var activity = Core.Tracing.StartActivity("CanSort10GbFile");
            var inputFile = $"{Path.GetRandomFileName()}.txt";
            var outputFile = $"{Path.GetRandomFileName()}.txt";
            try
            {
                Generator.GenerateFile(inputFile, GB * 10);
                Core.SortLargeFile(new SortingArgs { 
                    SourceFile = inputFile, 
                    DestinationFile = outputFile
                    // , TempDir = "R:\\tmp1" 
                });
                var isSorted = IsSortedFile(outputFile, activity);
                Assert.IsTrue(isSorted);
            }
            finally
            {
                if (File.Exists(inputFile))
                {
                    File.Delete(inputFile);
                }
                if (File.Exists(outputFile))
                {
                    File.Delete(outputFile);
                }
            }
        }

        public bool IsSortedFile(string file, Activity parent)
        {
            using var activity = Core.Tracing.StartActivity("IsSortedFile");
            activity.SetParentId(parent.Id);
            (long, string)? prevItem = null;
            using (var f = File.Open(file, FileMode.Open, FileAccess.Read))
            using (var s = new StreamReader(f, Encoding.Unicode))
            {
                var line = s.ReadLine();
                while (line != null)
                {
                    var parsed = Input.ParseLine(line);
                    if (prevItem.HasValue)
                    {
                        if (Core.Comparer.Compare(parsed, prevItem.Value) < 0) // found out of order items
                        {
                            return false;
                        }
                    }

                    prevItem = parsed;
                    line = s.ReadLine();
                }
            }

            return true;
        }
    }
}
