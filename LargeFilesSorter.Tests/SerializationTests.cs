﻿using NUnit.Framework;
using System.IO;
using System.Linq;


namespace LargeFilesSorter.Tests
{
    [TestFixture]
    public class SerializationTests
    {

        [Test]
        public void CenSerializeItems()
        {
            var sampleData = Enumerable.Range(0, 10000).Select(_ => Generator.GenerateItem()).ToArray();

            using (var m = new MemoryStream())
            {
                Serialization.SerializeItems(sampleData, m);
                m.Flush();

                m.Seek(0, SeekOrigin.Begin);

                var deserialized = m.DeserializeItems().ToArray();

                CollectionAssert.AreEqual(sampleData, deserialized);
            }
        }
    }
}
