﻿using System;

namespace LargeFilesSorter.Utils
{
    public static class DataSizeParser
    {
        public static DataSizeUnits.DataSize Parse(string size)
        {
            var nums = "";
            var tail = "";

            var numsEnded = false;
            foreach (var c in size)
            {
                if (!numsEnded && ((c >= '0' && c <= '9') || c == '.' || c == ',' || char.IsWhiteSpace(c)))
                {
                    if (!char.IsWhiteSpace(c))
                    {
                        nums = nums + $"{c}";
                    }
                }
                else
                {
                    numsEnded = true;
                    tail = tail + $"{c}";
                }
            }

            var sz = double.Parse(nums, System.Globalization.NumberStyles.Number);
            var u = DataSizeUnits.DataSize.ParseUnit(tail.Trim());
            return new DataSizeUnits.DataSize(sz, u);

        }

        public static long ToBytes(string size)
        {
            return Convert.ToInt64(Parse(size).ConvertToUnit(DataSizeUnits.Unit.Byte).Quantity);
        }
    }
}
