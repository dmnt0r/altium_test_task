## Building & testing.

See [build.cmd](build.cmd)

Some tests create and then delete files in %TEMP% directory.

## Running TestFileGenerator

This command line will generate a 1GB test file in Unicode encoding into gen-output.txt.

```TestFileGenerator\bin\Release\netcoreapp3.1\publish\TestFileGenerator.exe -s 1GB -o gen-output.txt```

## Running FileSorter

This command line will sort a Unicode gen-output.txt file into sorted.txt.  

```FileSorter\bin\Release\netcoreapp3.1\publish\FileSorter.exe -i gen-output.txt -o sorted.txt --max-chunk-size 100MB --max-breadth 16 --temp-folder R:\tmp```

First the file is broken into chunks of specified max-chunk-size (default is 256MB).  
Each chunk is sorted independently and then stored in temp directory (default is system's %TEMP%) under a random file name.  
Then all chunks will be merged into bigger chunk files, reading simultaneously at most max-breadth (default is 100) chunk files.  
At the final merge iteration, all temporary chunk files are merged and saved in the output file.  

### Tuning for various storage types

SSD and RAID types of storages support lower io latency on random access.  
I suggest using relatively small max-chunk-size (256MB is fine) and keep max-breadth at 100.  
  
For a single HDD storage a large number of simultaneously accessed files may introduce significant disk io performance penalty, which can be mitigated by tuning the --disk-buffer-size (-d) option, which is by 10MB by default.  
Setting bigger max-chunk-size (I suggest 1GB, but keep in mind that chunks are sorted in-memory) and lower max-breadth (16) will decrease disk io latency but will result in more file merging iterations.  


### Benchmark results

|	Storage	|	Input size	|	Chunk size	|	Max merge breadth	|	IO Buffer size	|	RAM Consumed	|	Run time   |
|	-----------	|	-----------	|	-----------	|	-----------	|	-----------	|	-----------	|	----------- |
|	HDD		|	10GB		|	1GB			|	100					|	256MB			|	4-7GB			|	00:04:25   |
|	HDD		|	10GB		|	1GB			|	100					|	64MB			|	4-7GB			|	00:04:23   |
|	HDD		|	10GB		|	1GB			|	100					|	16MB			|	4-7GB			|	00:04:20   |
|	HDD		|	10GB		|	256MB		|	100					|	256MB			|	2GB				|	00:04:10   |
|	HDD		|	10GB		|	256MB		|	100					|	64MB			|	2GB				|	00:04:08   |
|	HDD		|	10GB		|	256MB		|	100					|	16MB			|	2GB				|	00:04:09   |
|	HDD		|	10GB		|	256MB		|	16					|	256MB			|	2GB				|	00:05:14   |
|	HDD		|	10GB		|	256MB		|	16					|	64MB			|	2GB				|	00:05:04   |
|	HDD		|	10GB		|	256MB		|	16					|	16MB			|	2GB				|	00:05:04   |
|	HDD		|	10GB		|	64MB		|	100					|	256MB			|	2GB				|	00:04:51   |
|	HDD		|	10GB		|	64MB		|	100					|	64MB			|	1-2GB			|	00:04:45   |
|	HDD		|	10GB		|	64MB		|	100					|	16MB			|	1GB				|	00:04:48   |
|	HDD		|	10GB		|	64MB		|	16					|	256MB			|	2GB				|	00:05:31   |
|	HDD		|	10GB		|	64MB		|	16					|	64MB			|	1-2GB			|	00:05:32   |
|	HDD		|	10GB		|	64MB		|	16					|	16MB			|	1GB				|	00:05:29   |
|	SSD		|	10GB		|	1GB			|	100					|	256MB			|	4-7GB			|	00:01:36   |
|	SSD		|	10GB		|	1GB			|	100					|	64MB			|	4-7GB			|	00:01:37   |
|	SSD		|	10GB		|	1GB			|	100					|	16MB			|	4-7GB			|	00:01:36   |
|	SSD		|	10GB		|	256MB		|	100					|	256MB			|	2GB				|	00:01:39   |
|	SSD		|	10GB		|	256MB		|	100					|	64MB			|	2GB				|	00:01:42   |
|	SSD		|	10GB		|	256MB		|	100					|	16MB			|	2GB				|	00:01:39   |
|	SSD		|	10GB		|	256MB		|	16					|	256MB			|	2GB				|	00:02:03   |
|	SSD		|	10GB		|	256MB		|	16					|	64MB			|	2GB				|	00:02:05   |
|	SSD		|	10GB		|	256MB		|	16					|	16MB			|	2GB				|	00:02:02   |
|	SSD		|	10GB		|	64MB		|	100					|	256MB			|	2GB				|	00:02:02   |
|	SSD		|	10GB		|	64MB		|	100					|	64MB			|	1-2GB			|	00:02:02   |
|	SSD		|	10GB		|	64MB		|	100					|	16MB			|	1-2GB			|	00:02:02   |
|	SSD		|	10GB		|	64MB		|	16					|	256MB			|	1GB				|	00:02:16   |
|	SSD		|	10GB		|	64MB		|	16					|	64MB			|	1GB				|	00:02:17   |
|	SSD		|	10GB		|	64MB		|	16					|	16MB			|	1GB				|	00:02:15   |
|	HDD		|	100GB		|	256MB		|	100					|	10MB			|	10GB			|	01:40:11   |