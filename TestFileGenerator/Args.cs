﻿using CommandLine;
namespace TestFileGenerator
{
    public class Args
    {
        [Option('o', "output", Required = true, HelpText = "Output file")]
        public string OutputFile { get; set; }

        [Option('s', "size", Required = false, HelpText = "Desired file size", Default = "1GB")]
        public string Size { get; set; }
    }
}
