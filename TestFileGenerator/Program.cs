﻿using CommandLine;
using LargeFilesSorter.Utils;
using System;
namespace TestFileGenerator
{
    class Program
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Args>(args).WithParsed(Run);
        }

        static void Run(Args args)
        {
            try
            {
                var size = DataSizeParser.Parse(args.Size);
                var sizeStr = size.ToString();
                var sizeBytes = DataSizeParser.ToBytes(args.Size);
                var fullPath = System.IO.Path.GetFullPath(args.OutputFile);
                Console.WriteLine($"Will generate a Unicode text {sizeStr} file '{fullPath}'");
                LargeFilesSorter.Generator.GenerateFile(fullPath, sizeBytes);
                Console.WriteLine("Done.");
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Failed to generate:\n{e}");
                Environment.ExitCode = -1;
            }
        }

    }
}
